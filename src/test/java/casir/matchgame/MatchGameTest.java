package casir.matchgame;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.Exception;

class MatchGameTest
{
    
    @BeforeEach
        // this function is called before each test
    void setUp()
    {
        // init context
    }
    
    @Test
    void testRemoveMatches()
    {
        MatchGame g = new MatchGame(20, GameConfiguration.GetDefaultConfig());
        g.removeMatches(3);
        assertEquals(g.getRemainingMatches(), 17);
    }
    
    @Test
    void testRemoveMatchesWithInvalidArgs()
    {
        Executable assignation = () ->
        {
            MatchGame g = new MatchGame(20, GameConfiguration.GetDefaultConfig());
            g.removeMatches(-2);
        };
        
        assertThrows(IllegalArgumentException.class, assignation);
    }
    
    @Test
    void testIsStartedGameOver()
    {
        MatchGame g = new MatchGame(20, GameConfiguration.GetDefaultConfig());
        assertEquals(g.isGameOver(), false);
    }
    
    @Test
    void testRemoveMatchesWithAmountSuperiorTo3()
    {
        Executable assignation = () ->
        {
            MatchGame g = new MatchGame(20, GameConfiguration.GetDefaultConfig());
            g.removeMatches(4);
        };
        
        assertThrows(IllegalArgumentException.class, assignation);
    }
    
    @Test
    void testSmartComputerPlayerWithPlentyOfMatches()
    {
        SmartComputerPlayer player = new SmartComputerPlayer();
        int amountChoosen = player.askMatchesRemovalAmount(20);
        
        assertEquals(amountChoosen, 3);
    }
    
    @Test
    void testSmartComputerPlayerWith7Matches()
    {
        SmartComputerPlayer player = new SmartComputerPlayer();
        
        //It should enter in danger as the player can win at the next move.
        int amountChoosen = player.askMatchesRemovalAmount(7);
        
        assertEquals(amountChoosen, 1);
    }
    
    @Test
    void testSmartComputerPlayerWith5Matches()
    {
        SmartComputerPlayer player = new SmartComputerPlayer();
        
        int amountChosen = player.askMatchesRemovalAmount(5);
        
        assertEquals(amountChosen, 1);
    }
    
    @Test
    void testSmartComputerPlayerWith4Matches()
    {
        SmartComputerPlayer player = new SmartComputerPlayer();
        
        //The CPU can win
        int amountChoosen = player.askMatchesRemovalAmount(4);
        
        assertEquals(amountChoosen, 3);
    }
    
    @Test
    void testSmartComputerPlayerWith2Matches()
    {
        SmartComputerPlayer player = new SmartComputerPlayer();
        
        //The CPU can win and will choose 1.
        int amountChoosen = player.askMatchesRemovalAmount(2);
        
        assertEquals(amountChoosen, 1);
    }
    
    @Test
    void testConcatString()
    {
        assertEquals(MatchGameDisplay.repeatChar('1', 4), "1111");
    }
    
    @Test
    void testGetName()
    {
        ComputerPlayer a = new ComputerPlayer("Roger");
        assertEquals(a.getName(), "Roger");
        
        KeyboardPlayer p = new KeyboardPlayer("Luigi");
        assertEquals(p.getName(), "Luigi");
        
        SmartComputerPlayer s = new SmartComputerPlayer("Test");
        assertEquals(s.getName(), "Test");
    }
    
    @Test
    void testSetWinner()
    {
        IPlayer[] players = new IPlayer[]{new ComputerPlayer(), new KeyboardPlayer("Test"), new SmartComputerPlayer()};
        
        for (IPlayer p : players)
        {
            p.setWinner();
            assertEquals(p.hasWon(), true);
        }
    }
    
    @Test
    void testSetDisplayer()
    {
        IDisplay display = new MatchGameDisplay();
        MatchGame a = new MatchGame(20, GameConfiguration.GetDefaultConfig(), display);
        assertEquals(a.getDisplayer(), display);
    }
    
    @Test
    void testPerformMove()
    {
        MatchGame a = new MatchGame(20, GameConfiguration.GetDefaultConfig());
        MockPlayer p = new MockPlayer(1);
        try
        {
            a.performMove(p);
            
            
            assertEquals(a.getRemainingMatches(), 19);
        }
        catch (InvalidPlayerActionException ex)
        {
            assertEquals(true, false);
        }
    }
    
    @Test
    void testPerformMoveWithInvalidPlayer()
    {
        Executable assignation = () ->
        {
            IPlayer p = new InvalidPlayer();
            MatchGame a = new MatchGame(20, new GameConfiguration(p,p));
    
    
            a.performMove(p);
        };
        
        assertThrows(InvalidPlayerActionException.class, assignation);
    }
}
