package casir.matchgame;

public class MockPlayer implements IPlayer
{
    private int constantChoice;
    private boolean hasWon = false;
    
    public MockPlayer(int constantChoice)
    {
        this.constantChoice = constantChoice;
    }
    
    @Override
    public int askMatchesRemovalAmount(int remainingMatches)
    {
        return 1;
    }
    
    @Override
    public void setWinner()
    {
        hasWon = true;
    }
    
    @Override
    public boolean hasWon()
    {
        return hasWon;
    }
    
    @Override
    public String getName()
    {
        return "Mock";
    }
}
