package casir.matchgame;

import java.util.Random;

public class ComputerPlayer implements IPlayer
{
    private boolean hasWon = false;
    private String name;
    
    public ComputerPlayer()
    {
    }
    
    public ComputerPlayer(String name)
    {
        this.name = name;
    }
    
    //Ne peut pas être testé, ce joueur renvoie un nombre aléatoire.
    @Override
    public int askMatchesRemovalAmount(int remainingMatches)
    {
        return new Random().nextInt(2) + 1;
    }
    
    @Override
    public void setWinner()
    {
        hasWon = true;
    }
    
    @Override
    public boolean hasWon()
    {
        return hasWon;
    }
    
    @Override
    public String getName()
    {
        return name == null
               ? "CPU"
               : name;
    }
}
