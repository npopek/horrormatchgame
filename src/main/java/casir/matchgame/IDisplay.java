package casir.matchgame;

public interface IDisplay
{
    void displayGame(MatchGame game);
    
    void displayTurn(IPlayer player);
    
    void displayWinner(IPlayer winningPlayer);
    
    void displayRemovedMatches(int amountRemoved, IPlayer from);
}
