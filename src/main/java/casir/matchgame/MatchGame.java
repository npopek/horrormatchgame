package casir.matchgame;

public class MatchGame
{
    private IPlayer player1;
    private IPlayer player2;
    
    private IDisplay displayer;
    
    private int matchesQuantity;
    private boolean bGameOver = false;
    
    public MatchGame(int matchesQuantity, GameConfiguration config)
    {
        this.matchesQuantity = matchesQuantity;
        
        this.player1 = config.getPlayer1();
        this.player2 = config.getPlayer2();
        
        this.displayer = new MatchGameDisplay();
    }
    
    public MatchGame(int matchesQuantity, GameConfiguration config, IDisplay displayer)
    {
        this(matchesQuantity, config);
        
        this.displayer = displayer == null
                         ? new MatchGameDisplay()
                         : displayer;
    }
    
    //Ne peut pas être testé car trop global.
    public void startGame() throws InvalidPlayerActionException
    {
        while (!isGameOver())
        {
            displayer.displayGame(this);
            displayer.displayTurn(player1);
            performMove(player1);
            
            if (isGameOver()) break;
            
            displayer.displayGame(this);
            displayer.displayTurn(player2);
            
            performMove(player2);
        }
        
        printWinner();
    }
    
    public boolean isGameOver()
    {
        return bGameOver;
    }
    
    //Ne peut pas être testé. Les joueurs renvoient un nombre qui ne peut pas être connu à l'avance.
    public void performMove(IPlayer player) throws InvalidPlayerActionException
    {
        try
        {
            int matchAmountToRemove = player.askMatchesRemovalAmount(matchesQuantity);
            displayer.displayRemovedMatches(matchAmountToRemove, player);
            removeMatches(matchAmountToRemove);
            
            if (this.matchesQuantity >= 2) return;
            
            bGameOver = true;
            boolean hasWon = this.matchesQuantity == 1;
            if (hasWon) player.setWinner();
        }
        catch (IllegalArgumentException ex)
        {
            throw new InvalidPlayerActionException(ex);
        }
    }
    
    
    public void removeMatches(int amount) throws IllegalArgumentException
    {
        if (amount < 1) throw new IllegalArgumentException("La quantité doit être supérieure à 1.");
        if (amount > 3) throw new IllegalArgumentException("La quantié de doit pas dépasser 3.");
        matchesQuantity -= amount;
        
        matchesQuantity = matchesQuantity < 0
                          ? 0
                          : matchesQuantity;
    }
    
    public int getRemainingMatches()
    {
        return matchesQuantity;
    }
    
    //Ne peut pas être testé. Affiche à l'écran.
    private void printWinner()
    {
        displayer.displayWinner(player1.hasWon()
                                ? player1
                                : player2);
    }
    
    public IDisplay getDisplayer()
    {
        return displayer;
    }
}