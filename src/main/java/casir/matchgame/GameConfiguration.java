package casir.matchgame;

public class GameConfiguration
{
    public static GameConfiguration GetDefaultConfig()
    {
        return new GameConfiguration(new KeyboardPlayer("Player"), new ComputerPlayer());
    }
    
    private IPlayer firstPlayer;
    private IPlayer secondPlayer;
    
    public GameConfiguration(IPlayer player1, IPlayer player2)
    {
        firstPlayer = player1;
        secondPlayer = player2;
    }
    
    public IPlayer getPlayer1()
    {
        return firstPlayer;
    }
    
    public IPlayer getPlayer2()
    {
        return secondPlayer;
    }
}
