package casir.matchgame;


//Cette classe ne peut absoluement pas être testée, elle affiche à l'écran.
public class MatchGameDisplay implements IDisplay
{
    @Override
    public void displayGame(MatchGame game)
    {
        System.out.println("Il reste " + game.getRemainingMatches());
        System.out.println(repeatChar('|', game.getRemainingMatches()));
    }
    
    @Override
    public void displayTurn(IPlayer player)
    {
        System.out.println("A toi, " + player.getName() + " !");
    }
    
    @Override
    public void displayWinner(IPlayer winningPlayer)
    {
        System.out.println(winningPlayer.getName() + " a gagné !");
    }
    
    @Override
    public void displayRemovedMatches(int amountRemoved, IPlayer from)
    {
        System.out.println(from.getName() + " a retiré " + amountRemoved + " allumettes !");
    }
    
    public static String repeatChar(char character, int amount)
    {
        String result = "";
        for (int i = 0; i < amount; i++)
        {
            result += character;
        }
        
        return result;
    }
}
