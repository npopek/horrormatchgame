package casir.matchgame;

public class InvalidPlayer implements IPlayer
{
    private boolean hasWon = false;
    
    @Override
    public int askMatchesRemovalAmount(int remainingMatches)
    {
        return 5;
    }
    
    @Override
    public void setWinner()
    {
        hasWon = true;
    }
    
    @Override
    public boolean hasWon()
    {
        return hasWon;
    }
    
    @Override
    public String getName()
    {
        return null;
    }
}
