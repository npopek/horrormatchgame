package casir.matchgame;

public class InvalidPlayerActionException extends Exception
{
    public InvalidPlayerActionException(Exception ex)
    {
        super("Le joueur a renvoyé un nombre d'allumette invalide.\n" + ex.getMessage());
    }
}
