package casir.matchgame;

public class Program
{
    //Ne peut pas être testé: c'est le main.
    public static void main(String... args) throws Exception
    {
        MatchGame game = new MatchGame(20,
                new GameConfiguration(new KeyboardPlayer("Vous"), new SmartComputerPlayer("Ordinateur")),
                new MatchGameDisplay()
                );
        
        game.startGame();
    }
}

