package casir.matchgame;

import java.security.InvalidAlgorithmParameterException;

public class SmartComputerPlayer implements IPlayer
{
    private boolean hasWon;
    private String name;
    
    public SmartComputerPlayer()
    {
    }
    
    public SmartComputerPlayer(String name)
    {
        this.name = name;
    }
    
    @Override
    public int askMatchesRemovalAmount(int remainingMatches) throws IllegalArgumentException
    {
        if (remainingMatches <= 1) throw new IllegalArgumentException("I can't play.");
        
        if (remainingMatches > 7) return 3;
        
        if (remainingMatches == 5 || remainingMatches > 5)
            return 1; //Is endanger. The other player can win the next turn.
        
        if (remainingMatches < 5) return remainingMatches - 1;
        
        return 1;
    }
    
    @Override
    public void setWinner()
    {
        hasWon = true;
    }
    
    @Override
    public boolean hasWon()
    {
        return hasWon;
    }
    
    @Override
    public String getName()
    {
        return name == null
               ? "CPU"
               : name;
    }
}
