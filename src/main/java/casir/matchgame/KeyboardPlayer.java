package casir.matchgame;

import java.util.Scanner;

public class KeyboardPlayer implements IPlayer
{
    private Scanner keyboardScanner;
    private String name;
    
    private boolean hasWon = false;
    
    public KeyboardPlayer(String name)
    {
        keyboardScanner = new Scanner(System.in);
        this.name = name;
    }
    
    //Ne peut pas être testé: demande une entrée clavier.
    @Override
    public int askMatchesRemovalAmount(int remainingMatches)
    {
        System.out.println("Entrez le nombre d'allumettes que vous souhaitez retirer.");
        while (true)
        {
            try
            {
                int amount = keyboardScanner.nextInt();
                if (amount < 1 || amount > 3)
                {
                    System.out.println("Vous devez entrer un chiffre entre 1 et 3");
                    continue;
                }
                
                return amount;
            }
            catch (Exception ex)
            {
                System.out.println("Vous devez saisir un chiffre.");
                resetScanner();
            }
        }
    }
    
    @Override
    public void setWinner()
    {
        hasWon = true;
    }
    
    @Override
    public boolean hasWon()
    {
        return hasWon;
    }
    
    @Override
    public String getName()
    {
        return name;
    }
    
    private void resetScanner()
    {
        keyboardScanner = new Scanner(System.in);
    }
}
