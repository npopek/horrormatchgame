package casir.matchgame;

public interface IPlayer
{
    int askMatchesRemovalAmount(int remainingMatches);
    
    void setWinner();
    
    boolean hasWon();
    
    String getName();
}
